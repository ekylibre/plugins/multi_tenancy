# frozen_string_literal: true

require_relative 'lib/ekylibre/multi_tenancy/version'

Gem::Specification.new do |spec|
  spec.name = 'ekylibre-multi_tenancy'
  spec.version = Ekylibre::MultiTenancy::VERSION
  spec.authors = ['Ekylibre developers']
  spec.email = ['dev@ekylibre.com']

  spec.summary = 'Apartment integration as a plugin for Ekylibre'
  spec.required_ruby_version = '>= 2.6.0'
  spec.homepage = 'https://www.ekylibre.com'
  spec.license = 'AGPL-3.0-only'

  spec.files = Dir.glob(%w[lib/**/* *.gemspec])

  spec.require_paths = ['lib']

  spec.add_dependency 'apartment', '~> 2.2.0'
  spec.add_dependency 'ekylibre-plugin_system', '~> 0.7.0'
  spec.add_dependency 'public_suffix', '~> 4.0.6'
  spec.add_dependency 'zeitwerk', '~> 2.4.0'

  spec.add_development_dependency 'bundler', '>= 1.17'
  spec.add_development_dependency 'minitest', '~> 5.14'
  spec.add_development_dependency 'minitest-reporters', '~> 1.4'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rubocop', '~> 1.11.0'
end
