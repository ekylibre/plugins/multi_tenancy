# frozen_string_literal: true

require 'apartment'
require 'ekylibre-plugin_system'
require 'zeitwerk'

module Ekylibre # :nodoc:
end

loader = Zeitwerk::Loader.for_gem
loader.ignore(__FILE__)
loader.ignore("#{__dir__}/multi_tenancy/rails/**/*.rb")
loader.ignore("#{__dir__}/multi_tenancy/testing/**/*.rb")
loader.setup

require_relative 'ekylibre/multi_tenancy/rails/railtie' if defined?(::Rails)
