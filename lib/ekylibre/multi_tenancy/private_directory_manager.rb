# frozen_string_literal: true

using Corindon::DependencyInjection::Injectable
using Corindon::Result::Ext

module Ekylibre
  module MultiTenancy
    # Creates a subfolder for each tenant.
    # The name of the tenant is written at the root of the folder in a FLAG_FILE so that we only delete folders that we created
    class PrivateDirectoryManager
      FLAG_FILE_NAME = '.private_directory'

      injectable do
        args(
          private_root: MultiTenancyPlugin::PRIVATE_ROOT,
        )
      end

      # @param [Pathname] private_root
      def initialize(private_root:)
        @private_root = private_root
      end

      # @api
      # @param [String] name
      # @return [Pathname]
      def get(name)
        private_root.join(name)
      end

      # @api
      # @param [String] name
      # @return [Boolean]
      def has?(name)
        get(name).exist?
      end

      # @param [String] from
      # @param [String] to
      # @return [Corindon::Result::Result<Pathname, StandardError>]
      def rename(from, to:)
        if has?(to)
          Failure(StandardError.new("Private directory #{to} already exist"))
        elsif !has?(from)
          Failure(StandardError.new("Private directory #{from} does not exist"))
        else
          rescue_failure do
            private_to = get(to)
            get(from).rename(private_to)

            Success(private_to)
          end
        end
      end

      # @api
      # @param [String] name
      # @return [Corindon::Result::Result<Pathname, StandardError>]
      def create(name)
        rescue_failure do
          path = get(name)

          if !path.exist?
            path.mkdir
            path.join(FLAG_FILE_NAME).write(name)
          end

          Success(path)
        end
      end

      # @api
      # @param [String] name
      # @return [Corindon::Result::Result<Boolean, StandardError>]
      #   The result is True if the folder was removed. False if it was not removed due to missing FLAG_FILE
      def remove(name)
        dir = get(name)

        if dir.exist? && dir.directory?
          flag_file = dir.join(FLAG_FILE_NAME)

          if flag_file.exist? && flag_file.read == name
            FileUtils.rm_rf(dir)

            Success(true)
          else
            Success(false)
          end
        else
          Failure(StandardError.new("Private directory #{name} does not exist"))
        end
      end

      # @api
      # @param [String] name
      # @return [Corindon::Result::Result<Pathname, StandardError>]
      def ensure(name)
        if has?(name)
          Success(get(name))
        else
          create(name)
        end
      end

      private

        # @return [Pathname]
        attr_reader :private_root
    end
  end
end
