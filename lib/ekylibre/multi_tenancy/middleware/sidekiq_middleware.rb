# frozen_string_literal: true

# Heavily inspired from apartment-sidekiq
module Ekylibre
  module MultiTenancy
    module Middleware
      # Module containing Client ans Server Sidekiq middlewares for the MultiTenancy plugin
      module SidekiqMiddleware
        class << self
          def setup
            ::Sidekiq.configure_client do |config|
              configure_client(config)
            end

            ::Sidekiq.configure_server do |config|
              configure_client(config)

              configure_server(config)
            end
          end

          private

            def configure_client(config)
              config.client_middleware do |chain|
                chain.insert_after(
                  Ekylibre::PluginSystem::Middleware::SidekiqMiddleware::ClientMiddleware,
                  ContainerAwareClientMiddleware
                )
              end
            end

            def configure_server(config)
              config.server_middleware do |chain|
                chain.insert_after(
                  Ekylibre::PluginSystem::Middleware::SidekiqMiddleware::ServerMiddleware,
                  ContainerAwareServerMiddleware
                )
              end
            end
        end
      end
    end
  end
end
