# frozen_string_literal: true

require 'rack/request'

# Heavily inspired from Apartment::Elevators::Generic but with the ability to
module Ekylibre
  module MultiTenancy
    module Middleware
      #   Provides a rack based tenant switching solution based on request
      #
      class ContainerAwareRackMiddleware
        class << self
          attr_writer :excluded_subdomains

          def excluded_subdomains
            @excluded_subdomains ||= []
          end
        end

        def initialize(app)
          @app = app
        end

        def call(env)
          request = Rack::Request.new(env)
          container = env.fetch('container').dup

          PluginSystem::GlobalContainer.replace_with(container) do
            env['container'] = container

            database = extract_tenant(container, request)

            begin
              if database
                container
                  .get(TenantSwitcher)
                  .switch(database) { @app.call(env) }
              else
                @app.call(env)
              end
            rescue ::Apartment::TenantNotFound
              Rails.logger.error "Apartment Tenant not found: #{subdomain(request.host)}"
              return [404, { 'Content-Type' => 'text/html' }, [File.read(Rails.root.join('public', '404.html'))]]
            end
          end
        end

        # @private
        def extract_tenant(container, request)
          if (tenant = ENV['TENANT'])
            tenant
          elsif container.parameter(PluginSystem::Parameters::APPLICATION_ENV) == 'test'
            'test'
          elsif ENV['ELEVATOR'] == 'header'
            request.env['HTTP_X_TENANT']
          else
            extract_subdomain(request)
          end
        end

        private

          def extract_subdomain(request)
            request_subdomain = subdomain(request.host)

            # If the domain acquired is set to be excluded, set the tenant to whatever is currently
            # next in line in the schema search path.
            tenant = if self.class.excluded_subdomains.include?(request_subdomain)
                       nil
                     else
                       request_subdomain
                     end

            tenant.presence
          end

          # *Almost* a direct ripoff of ActionDispatch::Request subdomain methods

          # Only care about the first subdomain for the database name
          def subdomain(host)
            subdomains(host).first
          end

          def subdomains(host)
            host_valid?(host) ? parse_host(host) : []
          end

          def host_valid?(host)
            !ip_host?(host) && domain_valid?(host)
          end

          def ip_host?(host)
            !/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/.match(host).nil?
          end

          def domain_valid?(host)
            PublicSuffix.valid?(host, ignore_private: true)
          end

          def parse_host(host)
            (PublicSuffix.parse(host, ignore_private: true).trd || '').split('.')
          end
      end
    end
  end
end
