# frozen_string_literal: true

# Heavily inspired from apartment-sidekiq
module Ekylibre
  module MultiTenancy
    module Middleware
      module SidekiqMiddleware
        # Server Sidekiq middleware switching to the correct tenant (if any) based on the job context
        class ContainerAwareServerMiddleware
          def call(worker_class, item, queue)
            tenant = item.fetch('tenant', nil)
            container = item.fetch('container').dup

            PluginSystem::GlobalContainer.replace_with(container) do
              item['container'] = container

              if tenant.nil?
                yield
              else
                container
                  .get(TenantSwitcher)
                  .switch(tenant) { yield }
              end
            end
          end
        end
      end
    end
  end
end
