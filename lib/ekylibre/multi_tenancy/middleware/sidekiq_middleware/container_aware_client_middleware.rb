# frozen_string_literal: true

# Heavily inspired from apartment-sidekiq
module Ekylibre
  module MultiTenancy
    module Middleware
      module SidekiqMiddleware
        # Client Sidekiq middleware to add the name of the current tenant to the job context
        class ContainerAwareClientMiddleware
          def call(worker_class, item, queue, redis_pool = nil)
            item['tenant'] ||= item.fetch('container').get(TenantStack).current&.name

            yield
          end
        end
      end
    end
  end
end
