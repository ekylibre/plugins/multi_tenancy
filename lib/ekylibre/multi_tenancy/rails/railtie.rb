# frozen_string_literal: true

module Ekylibre
  module MultiTenancy
    module Rails
      # Railtie to integrate the MultiTenancy Plugin into a Rails application
      class Railtie < ::Rails::Railtie
        extend PluginSystem::PluginRegistration

        register_plugin(MultiTenancyPlugin)

        initializer(:register_middleware, after: :engines_blank_point) do |app|
          app.config.middleware.insert_after(
            Ekylibre::PluginSystem::Middleware::RackMiddleware,
            Ekylibre::MultiTenancy::Middleware::ContainerAwareRackMiddleware
          )

          Middleware::SidekiqMiddleware.setup if defined?(::Sidekiq)
        end

        console do
          ::Rails::ConsoleMethods.send :include, ConsoleHelper
        end
      end
    end
  end
end
