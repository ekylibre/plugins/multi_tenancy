# frozen_string_literal: true

module Ekylibre
  module MultiTenancy
    # Stack allowing to track which is the current tenant across multiple nested contexts
    class TenantStack
      def initialize
        @stack = []
      end

      # @api
      # @return [Tenant, nil]
      def current
        @stack.last
      end

      def pop
        @stack.pop

        nil
      end

      # @param [Tenant] tenant
      def push(tenant)
        @stack << tenant
      end

      def size
        @stack.size
      end
    end
  end
end
