# frozen_string_literal: true

module Ekylibre
  module MultiTenancy
    # Helper methods to switch tenants in the Rails console
    module ConsoleHelper
      # @param [String] name
      def switch_tenant!(name)
        container.get(TenantSwitcher)
                 .switch!(name)
      end

      def leave_tenant!
        container.get(TenantSwitcher)
                 .leave!
      end

      # @param [String] name
      def switch_tenant(name, &block)
        return if block.nil?

        container.get(TenantSwitcher)
                 .switch(name) { block.call }
      end
    end
  end
end
