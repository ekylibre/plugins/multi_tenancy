# frozen_string_literal: true

using Corindon::DependencyInjection::Injectable

module Ekylibre
  module MultiTenancy
    # Plugin adding multi-tenancy to Ekylibre
    class MultiTenancyPlugin < Ekylibre::PluginSystem::Plugin
      injectable do
        args(
          app: Ekylibre::PluginSystem::Parameters::RAILS_APPLICATION,
          engine: param(Ekylibre::MultiTenancy::Rails::Railtie),
        )
        tag 'ekylibre.system.plugin'
      end

      APARTMENT = make_parameter('apartment')
      DATABASE = make_definition('database', factory(APARTMENT, :database))
      PRIVATE_ROOT = make_parameter('private_root')
      TENANTS_FILE = make_parameter('tenants_file')

      def initialize(app:, engine:)
        super(engine: engine)

        @app = app
      end

      # @param [Corindon::DependencyInjection::Container] container
      def boot(container)
        container.set_parameter(APARTMENT, ::Apartment::Tenant)
        container.set_parameter(PRIVATE_ROOT, app.root.join('private'))
        container.set_parameter(TENANTS_FILE, app.root.join('config', 'tenants.yml'))

        container.add_definition(DATABASE)
        container.add_definition(TenantRepository)
        container.add_definition(TenantStack)
        container.add_definition(TenantSwitcher)
        container.add_definition(TenantsFileLoader)
      end

      def version
        MultiTenancy::VERSION
      end

      private

        # @return [::Rails::Application]
        attr_reader :app
    end
  end
end
