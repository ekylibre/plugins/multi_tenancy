# frozen_string_literal: true

module Ekylibre
  module MultiTenancy
    module Mixin
      module TestCaseMixin
        class << self
          def included(base)
            if !base.ancestors.include?(PluginSystem::Mixin::TestCaseMixin)
              raise StandardError.new('This mixin requires the TestCaseMixin from the PluginSystem to be _prepended_ in the TestCase class')
            end
          end
        end

        def before_setup
          container.get(TenantSwitcher).switch!(testcase_tenant)

          super
        end

        def after_teardown
          super
        ensure
          container.get(TenantSwitcher).leave!
        end

        # Redefine this method in your test case to use an other tenant for all the tests in it.
        # It can also be defined dynamically based on the name of the test executed.
        def testcase_tenant
          'test'
        end
      end
    end
  end
end
