# frozen_string_literal: true

module Ekylibre
  module MultiTenancy
    module Testing
      class MockApartment
        def switch(*, **)
          yield if block_given?
        end

        def switch!(*, **) end

        def create(*, **) end

        def drop(*, **) end
      end
    end
  end
end
