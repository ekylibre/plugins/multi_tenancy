# frozen_string_literal: true

using Corindon::DependencyInjection::Injectable

module Ekylibre
  module MultiTenancy
    # Service allowing to switch between tenants and keeping track of what need to be cleaned through the use of a TenantStack
    class TenantSwitcher
      injectable(
        apartment: MultiTenancyPlugin::APARTMENT,
        tenant_stack: TenantStack,
        tenant_repository: TenantRepository,
      )

      # @param [#switch, #switch!] apartment
      # @param [TenantStack] tenant_stack
      # @param [TenantRepository] tenant_repository
      def initialize(apartment:, tenant_stack:, tenant_repository:)
        @apartment = apartment
        @tenant_stack = tenant_stack
        @tenant_repository = tenant_repository
        @bang_stack = []
        @bang_switched = false
      end

      # @api
      # @param [String, #to_s] name
      def switch(name, &block)
        name = name.to_s

        tenant = tenant_repository.get(name)
        return if tenant.nil?

        begin
          @bang_stack << @bang_switched
          @bang_switched = false
          tenant_stack.push(tenant)

          apartment.switch(name, &block)
        ensure
          # Make sure we pop the stack twice if we bang_switched
          leave! if @bang_switched
          @bang_switched = @bang_stack.pop
          tenant_stack.pop
        end
      end

      # @api
      # @param [String, #to_s] name
      def switch!(name)
        name = name.to_s
        tenant = tenant_repository.get(name)
        return if tenant.nil?

        # Some housekeeping to keep the tenant_stack up-to-date
        tenant_stack.pop if @bang_switched
        @bang_switched = true
        tenant_stack.push(tenant)

        apartment.switch!(name)
      end

      # @api
      def leave!
        if @bang_switched
          @bang_switched = false
          tenant_stack.pop
          apartment.switch!(tenant_stack.current)
        else
          raise StandardError.new('Cannot leave! if not after switch!')
        end
      end

      private

        # @return [#switch, #switch!]
        attr_reader :apartment
        # @return [TenantRepository]
        attr_reader :tenant_repository
        # @return [TenantStack]
        attr_reader :tenant_stack
    end
  end
end
