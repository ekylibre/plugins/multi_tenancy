# frozen_string_literal: true

using Corindon::DependencyInjection::Injectable

module Ekylibre
  module MultiTenancy
    class TenantRepository
      injectable do
        args(
          private_root: MultiTenancyPlugin::PRIVATE_ROOT,
          tenants: on(TenantsFileLoader).call(:load),
        )
      end

      # @param [Pathname] private_root
      def initialize(private_root:, tenants:)
        @private_root = private_root
        @tenants = tenants
        @write_mutex = Mutex.new
      end

      # @api
      # @param [String] name
      # @return [Tenant, nil]
      def get(name)
        if has?(name)
          Tenant.new(name: name, private_directory: private_root.join(name))
        else
          nil
        end
      end

      # @param [String] name
      def add(name)
        tenants << name
      end

      # @param [String] name
      def delete(name)
        tenants.delete(name)
      end

      # @api
      # @param [String] name
      # @return [Boolean]
      def has?(name)
        list.include?(name)
      end

      # @api
      # @return [Array<String>]
      def list
        tenants
      end

      private

        # @return [Array<String>]
        attr_reader :tenants
        # @return [Pathname]
        attr_reader :private_root
        # @return [Pathname]
        attr_reader :tenants_file
        # @return [Mutex]
        attr_reader :write_mutex
    end
  end
end
