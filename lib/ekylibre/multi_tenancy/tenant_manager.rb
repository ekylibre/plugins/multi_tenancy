# frozen_string_literal: true

using Corindon::DependencyInjection::Injectable
using Corindon::Result::Ext

module Ekylibre
  module MultiTenancy
    class TenantManager
      injectable do
        args(
          apartment: MultiTenancyPlugin::APARTMENT,
          private_dir_manager: PrivateDirectoryManager,
          tenant_loader: TenantsFileLoader,
          tenant_repository: TenantRepository,
        )
      end

      # @param [TenantRepository] tenant_repository
      # @param [TenantsFileLoader] tenant_loader
      # @param [Apartment::Tenant] apartment
      # @param [PrivateDirectoryManager] private_dir_manager
      def initialize(tenant_repository:, tenant_loader:, apartment:, private_dir_manager:)
        @tenant_repository = tenant_repository
        @tenant_loader = tenant_loader
        @apartment = apartment
        @private_dir_manager = private_dir_manager
      end

      # @api
      # @param [String] name
      # @return [Corindon::Result::Result<Tenant, StandardError>]
      def create(name)
        if tenant_repository.has?(name)
          Failure(StandardError.new("Tenant with name '#{name}' already exists."))
        else
          add(name)
          apartment.create(name)

          make_tenant(name)
        end
      end

      # @private
      # @param [String] name
      def add(name)
        tenant_repository.add(name)
        tenant_loader.write(tenant_repository.list)
      end

      # @api
      # @param [String] name
      # @return [Corindon::Result::Result<Tenant, StandardError>]
      def ensure(name)
        if tenant_repository.has?(name)
          Success(tenant_repository.get(name))
        else
          create(name)
        end
      end

      # @api
      # @param [Tenant] tenant
      # @return [Corindon::Result::Result<nil, StandardError>]
      def drop(tenant)
        rescue_failure do
          private_dir_manager.remove(tenant.name).unwrap!

          tenant_repository.delete(tenant.name)
          tenant_loader.write(tenant_repository.list)

          apartment.drop(tenant.name)

          Success(nil)
        end
      end

      private

        # @return [TenantRepository]
        attr_reader :tenant_repository
        # @return [TenantsFileLoader]
        attr_reader :tenant_loader
        # @return [Apartment::Tenant]
        attr_reader :apartment
        # @return [PrivateDirectoryManager]
        attr_reader :private_dir_manager

        # @param [String] name
        def make_tenant(name)
          rescue_failure do
            private_directory = private_dir_manager.ensure(name).unwrap!

            Success(Tenant.new(name: name, private_directory: private_directory))
          end
        end
    end
  end
end
