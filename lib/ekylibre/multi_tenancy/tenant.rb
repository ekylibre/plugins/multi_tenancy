# frozen_string_literal: true

using Corindon::DependencyInjection::Injectable

module Ekylibre
  module MultiTenancy
    # Represents a tenant in the MultiTenancy plugin
    class Tenant
      # @api
      # @return [String]
      attr_reader :name
      # @api
      # @return [Pathname]
      attr_reader :private_directory

      # @param [String] name
      # @param [Pathname] private_directory
      def initialize(name:, private_directory:)
        @name = name
        @private_directory = private_directory
      end
    end
  end
end
