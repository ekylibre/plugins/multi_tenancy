# frozen_string_literal: true

using Corindon::DependencyInjection::Injectable

module Ekylibre
  module MultiTenancy
    class TenantsFileLoader
      injectable(env: PluginSystem::Parameters::APPLICATION_ENV, file: MultiTenancyPlugin::TENANTS_FILE)

      # @param [String] env
      # @param [Pathname] file
      def initialize(env:, file:)
        @env = env
        @file = file

        @write_mutex = Mutex.new
      end

      # @return [Array<String>]
      def load
        load_file.fetch(env, [])
      end

      # @param [Array<String>] tenants
      def write(tenants)
        write_mutex.synchronize do
          data = load_file.merge(env => tenants)

          file.write(YAML.dump(data))
        end

        nil
      end

      private

        # @return [String]
        attr_reader :env
        # @return [Pathname]
        attr_reader :file
        # @return [Mutex]
        attr_reader :write_mutex

        # @return [Hash{String=>Array<String>}]
        def load_file
          if File.exist?(file)
            YAML.load_file(file) || {}
          else
            {}
          end
        end
    end
  end
end
