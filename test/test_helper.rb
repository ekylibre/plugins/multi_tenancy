# frozen_string_literal: true

require_relative '../lib/ekylibre-multi_tenancy'
require_relative '../lib/ekylibre/multi_tenancy/testing/mock_apartment'

require 'ekylibre/plugin_system/testing/spec_reporter'

require 'minitest/autorun'

Minitest::Reporters.use! Ekylibre::PluginSystem::Testing::SpecReporter.new
