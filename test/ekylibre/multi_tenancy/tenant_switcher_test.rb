# frozen_string_literal: true

require 'test_helper'

describe Ekylibre::MultiTenancy::TenantSwitcher do
  before do
    @repository = Ekylibre::MultiTenancy::TenantRepository.new(
      private_root: Pathname.new(''),
      tenants: []
    )
    @stack = Ekylibre::MultiTenancy::TenantStack.new
    @switcher = Ekylibre::MultiTenancy::TenantSwitcher.new(
      apartment: Ekylibre::MultiTenancy::Testing::MockApartment.new,
      tenant_repository: @repository,
      tenant_stack: @stack
    )
  end

  describe :switch do
    it 'does not call the given block if no tenant are found' do
      @repository.stub(:get, nil) do
        called = false
        @switcher.switch('does_not_exist') { called = true }

        refute called
      end
    end

    it 'pushes the tenant on the stack and call the given block if the tenant is found' do
      @repository.stub(:has?, true) do
        called = false

        @switcher.switch('exists') do
          called = true
          assert_equal 'exists', @stack.current.name
        end

        assert called
      end
    end

    it 'pops back once on block exit' do
      @repository.stub(:has?, true) do
        @switcher.switch('found') do
          @switcher.switch('also_found') do
            assert_equal 'also_found', @stack.current.name
          end

          assert_equal 'found', @stack.current.name
        end
      end
    end
  end

  describe :switch! do
    it 'does not push the tenant on the stack if not found' do
      @repository.stub(:get, nil) do
        @switcher.switch!('not_found')

        assert_nil @stack.current
      end
    end

    it 'pushes the tenant on the stack if found' do
      @repository.stub(:has?, true) do
        @switcher.switch!('found')

        assert_equal 1, @stack.size
        assert_equal 'found', @stack.current.name
      end
    end
  end

  describe :switch_and_switch! do
    it 'switch pops twice if switch! is called inside a switch block' do
      @repository.stub(:has?, true) do
        @switcher.switch('found') do
          @switcher.switch!('also_found')
          @switcher.switch!('found_again')
        end

        assert @stack.size.zero?
      end
    end

    it 'switch pops once if switch! was called outside of the block' do
      @repository.stub(:has?, true) do
        @switcher.switch!('found')
        @switcher.switch('also_found') do
          assert_equal 2, @stack.size
        end

        assert_equal 1, @stack.size
        assert_equal 'found', @stack.current.name
      end
    end
  end

  describe :leave! do
    it 'raises an exception when calling leave! before calling switch!' do
      assert_raises('Cannot leave! if not after switch!') do
        @switcher.leave!
      end
    end
  end
end
