# frozen_string_literal: true

require 'test_helper'

using Corindon::DependencyInjection::Testing::MockUtils

module Ekylibre
  module MultiTenancy
    describe TenantManager do
      before do
        @temp_tenant_file = Tempfile.new
        File.write(@temp_tenant_file, <<~YAML)
          test:
            - toto_exists
        YAML

        @apartment = Testing::MockApartment.new

        @temp_private_dir_root = Pathname.new(Dir.mktmpdir)

        @container = Corindon::DependencyInjection::Container.new
        @container.set_parameter(PluginSystem::Parameters::APPLICATION_ENV, 'test')
        @container.set_parameter(MultiTenancyPlugin::TENANTS_FILE, Pathname.new(@temp_tenant_file.path))
        @container.set_parameter(MultiTenancyPlugin::PRIVATE_ROOT, @temp_private_dir_root)
        @container.add_definition(TenantManager)
        @container.add_definition(TenantsFileLoader)
        @container.add_definition(TenantRepository)
        @container.set_parameter(MultiTenancyPlugin::APARTMENT, @apartment)
      end

      after do
        if @temp_tenant_file && File.exist?(@temp_tenant_file)
          @temp_tenant_file.close
          @temp_tenant_file.unlink
        end

        if @temp_private_dir_root && @temp_private_dir_root.directory?
          FileUtils.rm_rf(@temp_private_dir_root)
        end
      end

      describe :create do
        before do
          @manager = @container.get(TenantManager)
        end

        it 'returns the newly created tenant' do
          assert_instance_of(Tenant, @manager.create('toto').unwrap!)
        end

        it 'fails if the tenant already exists' do
          refute(@manager.create('toto_exists').success?)
        end

        it 'updates the list of tenants' do
          assert @manager.create('toto').success?

          assert @container.get(TenantRepository).has?('toto')
        end

        it 'persists it through the tenant_loader' do
          @manager.create('new_toto')

          assert_includes @container.get(TenantsFileLoader).load, 'new_toto'
        end
      end

      describe :ensure do
        using Corindon::Result::Ext

        before do
          @manager = @container.get(TenantManager)
        end

        it 'does nothing if the tenant already exists' do
          @manager.stub(:create, -> { assert false, 'should not be called' }) do
            assert @manager.ensure('toto_exists').success?
          end
        end

        it 'creates the tenant if it does not exist' do
          called = false
          @manager.stub(:create, ->(name) {
            called = true

            Success(Tenant.new(name: name, private_directory: @temp_private_dir_root.join(name)))
          }) do
            assert @manager.ensure('toto').success?
          end

          assert called
        end
      end

      describe :with_mock_apartment do
        before do
          @apartment = Minitest::Mock.new
          @container.set_parameter(MultiTenancyPlugin::APARTMENT, @apartment)
        end

        after do
          assert_mock @apartment
        end

        describe :create do
          it 'creates the tenant database through apartment' do
            manager = @container.get(TenantManager)
            @apartment.expect(:create, nil, ['toto'])

            assert manager.create('toto').success?
          end
        end

        describe :drop do
          using Corindon::Result::Ext

          it 'drops the tenant database through apartment' do
            @apartment.expect(:drop, nil, ['toto_exists'])
            private_dir_manager = Minitest::Mock.new
            private_dir_manager.expect(:remove, Success(true), ['toto_exists'])
            @container.mock_definition(PrivateDirectoryManager, private_dir_manager)

            manager = @container.get(TenantManager)

            tenant = @container.get(TenantRepository).get('toto_exists')
            res = manager.drop(tenant)
            assert(res.success?, -> { res.error })
          end
        end
      end
    end
  end
end
