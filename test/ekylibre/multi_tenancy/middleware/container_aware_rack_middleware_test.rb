# frozen_string_literal: true

require 'test_helper'

using Corindon::DependencyInjection::Testing::MockUtils

def mock_env(variables = {}, &block)
  ENV.stub(:[], ->(key) { variables.fetch(key) { ENV[key] } }) do
    block.call
  end
end

module Ekylibre
  module MultiTenancy
    describe Middleware::ContainerAwareRackMiddleware do
      describe :extract_tenant do
        before do
          @middleware = Middleware::ContainerAwareRackMiddleware.new(->(_env) {})
          @container = Corindon::DependencyInjection::Container.new
          @container.set_parameter(PluginSystem::Parameters::APPLICATION_ENV, 'test')
        end

        it 'returns the value of the TENANT env var if present' do
          mock_env({ 'TENANT' => 'demo' }) do
            assert_equal('demo', @middleware.extract_tenant(@container, {}))
          end
        end

        it 'returns "test" if environment is test' do
          assert_equal('test', @middleware.extract_tenant(@container, {}))
        end
      end

      describe :call do
        before do
          @repository = Class.new do
            def get(name)
              Tenant.new(name: name, private_directory: nil)
            end
          end.new

          @container = Corindon::DependencyInjection::Container.new
          @container.add_definition(TenantStack)
          @container.add_definition(TenantSwitcher)
          @container.mock_definition(TenantRepository, @repository)
          @container.set_parameter(MultiTenancyPlugin::APARTMENT.key, Testing::MockApartment.new)

          @env = {
            'container' => @container
          }
        end

        it 'Should switch to the extracted tenant in the nested app' do
          called = false
          app = ->(env) {
            called = true
            tenant = env['container'].get(TenantStack).current
            assert tenant.is_a?(Tenant)
            assert_equal 'test', tenant.name
          }
          middleware = Middleware::ContainerAwareRackMiddleware.new(app)

          def middleware.extract_tenant(*, **)
            'test'
          end

          middleware.call(@env)
          assert called, 'The app was not called by the middleware'
        end

        # This test creates a thread that start a request and is blocked until a second concurrent request finishes.
        it 'Should not share the tenant stack between requests on multithreaded environments' do
          called = 0

          app = ->(env) {
            stack = env['container'].get(TenantStack)
            assert_equal 1, stack.size

            called += 1
          }

          router = ->(env) {
            if env.key?('lock')
              env.fetch('lock').synchronize {
                app.call(env)
              }
            else
              app.call(env)
            end
          }

          middleware = Middleware::ContainerAwareRackMiddleware.new(router)

          def middleware.extract_tenant(*, **)
            'test'
          end

          lock = Mutex.new
          t_wait = nil

          begin
            lock.synchronize do
              t_wait = Thread.new do
                middleware.call(@env.merge('lock' => lock))
              end

              # We must wait that `t_wait` is scheduled and blocked on t_wait
              sleep(0.1) until t_wait.stop?

              middleware.call(@env)
              assert_equal 1, called, 'Called should be 1 as the "first request" is still pending (we have the lock)'
            end
          ensure
            t_wait.join if t_wait
          end

          assert_equal 2, called, 'The app was not called like expected'
        end
      end
    end
  end
end
