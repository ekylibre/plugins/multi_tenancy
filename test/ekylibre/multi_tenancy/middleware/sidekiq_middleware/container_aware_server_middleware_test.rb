# frozen_string_literal: true

using Corindon::DependencyInjection::Testing::MockUtils

describe Ekylibre::MultiTenancy::Middleware::SidekiqMiddleware::ContainerAwareServerMiddleware do
  before do
    @repository = Ekylibre::MultiTenancy::TenantRepository.new(
      private_root: Pathname.new(''),
      tenants: [],
    )
    @container = Corindon::DependencyInjection::Container.new
    @container.set_parameter(Ekylibre::MultiTenancy::MultiTenancyPlugin::APARTMENT.key, Ekylibre::MultiTenancy::Testing::MockApartment.new)
    @container.add_definition(Ekylibre::MultiTenancy::TenantStack)
    @container.add_definition(Ekylibre::MultiTenancy::TenantSwitcher)
    @container.mock_definition(Ekylibre::MultiTenancy::TenantRepository, @repository)

    @env = {
      'container' => @container
    }
  end

  it 'yields if tenant is nil' do
    middleware = Ekylibre::MultiTenancy::Middleware::SidekiqMiddleware::ContainerAwareServerMiddleware.new

    called = false

    middleware.call(nil, (env = @env.merge('tenant' => nil)), nil) do
      called = true
      assert env['container'].get(Ekylibre::MultiTenancy::TenantStack).size.zero?
    end

    assert called
  end
  it 'yields if tenant key is not presentl' do
    middleware = Ekylibre::MultiTenancy::Middleware::SidekiqMiddleware::ContainerAwareServerMiddleware.new

    called = false

    middleware.call(nil, @env, nil) do
      called = true
      assert @env['container'].get(Ekylibre::MultiTenancy::TenantStack).size.zero?
    end

    assert called
  end

  it 'yields if tenant is provided' do
    middleware = Ekylibre::MultiTenancy::Middleware::SidekiqMiddleware::ContainerAwareServerMiddleware.new

    called = false

    @repository.stub(:has?, true) do
      env = @env.merge('tenant' => 'tenant_name')

      middleware.call(nil, env, nil) do
        called = true
        assert_equal 'tenant_name', env['container'].get(Ekylibre::MultiTenancy::TenantStack).current.name
      end
    end

    assert called
  end
end
