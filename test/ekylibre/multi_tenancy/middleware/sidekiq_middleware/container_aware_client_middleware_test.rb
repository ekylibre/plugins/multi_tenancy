# frozen_string_literal: true

describe Ekylibre::MultiTenancy::Middleware::SidekiqMiddleware::ContainerAwareClientMiddleware do
  before do
    @container = Corindon::DependencyInjection::Container.new
    @container.add_definition(Ekylibre::MultiTenancy::TenantStack)

    @container.get(Ekylibre::MultiTenancy::TenantStack).push(
      Ekylibre::MultiTenancy::Tenant.new(name: 'tenant_name', private_directory: Pathname.new(''))
    )
    @env = {
      'container' => @container
    }
  end

  it 'saves the current tenant name in the request environment and calls the given block' do
    middleware = Ekylibre::MultiTenancy::Middleware::SidekiqMiddleware::ContainerAwareClientMiddleware.new

    called = false

    middleware.call(nil, @env, nil, nil) do
      called = true
      assert @env.key?('tenant')
      assert_equal 'tenant_name', @env.fetch('tenant')
    end

    assert called
  end
end
