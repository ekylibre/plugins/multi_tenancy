# frozen_string_literal: true

require 'test_helper'

module Ekylibre
  module MultiTenancy
    describe TenantRepository do
      before do
        @repo = TenantRepository.new(
          private_root: Pathname.new(''),
          tenants: %w[existing]
        )
      end

      describe :has? do
        it 'returns false when the tenant is not in the list' do
          refute(@repo.has?(:toto))
        end

        it 'returns true when the tenant exists' do
          assert(@repo.has?('existing'))
        end
      end

      describe :get do
        it 'Returns an instance of Tenant with the correct name if the tenant exists' do
          tenant = @repo.get('existing')

          assert_instance_of(Tenant, tenant)
          assert_equal('existing', tenant.name)
        end

        it 'returns nil if the tenant does not exists' do
          assert_nil(@repo.get('toto'))
        end
      end

      describe :add do
        it 'adds the provided name to the list' do
          @repo.add('toto')

          assert(@repo.has?('toto'))
        end
      end

      describe :delete do
        it 'removes the tenant from the list' do
          @repo.delete('existing')

          refute @repo.has?('existing')
        end

        it 'works if the tenant is already absent' do
          @repo.delete('toto')
        end
      end
    end
  end
end
