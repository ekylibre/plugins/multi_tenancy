# frozen_string_literal: true

require 'test_helper'

using Corindon::DependencyInjection::Testing::MockUtils

module Ekylibre
  module MultiTenancy
    describe PrivateDirectoryManager do
      before do
        @temp_private_dir_root = Pathname.new(Dir.mktmpdir)

        @dir_manager = PrivateDirectoryManager.new(private_root: @temp_private_dir_root)
      end

      after do
        if @temp_private_dir_root && @temp_private_dir_root.directory?
          FileUtils.rm_rf(@temp_private_dir_root)
        end
      end

      describe :create do
        it 'creates the folder and a flag file inside it' do
          res = @dir_manager.create('toto')

          assert res.success?

          dir = res.unwrap!
          assert dir.exist?

          flag_file = dir.join(PrivateDirectoryManager::FLAG_FILE_NAME)

          assert flag_file.file?
          assert_equal 'toto', flag_file.read
        end

        it 'does not create the flag file if the folder already exist' do
          @temp_private_dir_root.join('toto').mkdir

          res = @dir_manager.create('toto')
          assert res.success?

          refute res.unwrap!.join(PrivateDirectoryManager::FLAG_FILE_NAME).exist?
        end
      end

      describe :remove do
        it 'removes the folder if the flag file is present' do
          dir = @temp_private_dir_root.join('toto')
          dir.mkdir
          dir.join(PrivateDirectoryManager::FLAG_FILE_NAME).write('toto')

          res = @dir_manager.remove('toto')
          assert res.success?
          assert res.unwrap!

          refute dir.exist?
        end

        it 'does not remove the directory if the flag file is not present' do
          dir = @temp_private_dir_root.join('toto')
          dir.mkdir

          refute @dir_manager.remove('toto').unwrap!
        end

        it 'returns failure if the directory does not exist' do
          res = @dir_manager.remove('toto')

          assert res.failure?
        end
      end
    end
  end
end
