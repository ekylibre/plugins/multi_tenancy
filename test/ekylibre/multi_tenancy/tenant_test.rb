# frozen_string_literal: true

require 'test_helper'

describe Ekylibre::MultiTenancy::Tenant do
  before do
    @tenant = Ekylibre::MultiTenancy::Tenant.new(name: 'tenant_name', private_directory: Pathname.new('42'))
  end

  describe :name do
    it 'returns the value provided to the constructor' do
      assert_equal 'tenant_name', @tenant.name
    end
  end

  describe :private_directory do
    it 'returns the value provided to the constructor' do
      assert_equal Pathname.new('42'), @tenant.private_directory
    end
  end
end
