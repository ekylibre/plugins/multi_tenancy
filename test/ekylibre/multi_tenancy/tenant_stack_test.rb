# frozen_string_literal: true

require 'test_helper'

describe Ekylibre::MultiTenancy::TenantStack do
  before do
    @stack = Ekylibre::MultiTenancy::TenantStack.new
  end

  describe :current do
    it 'returns nil when empty' do
      assert_nil @stack.current
    end

    it 'returns the last pushed value' do
      @stack.push(1)
      assert_equal 1, @stack.current

      @stack.push(2)
      assert_equal 2, @stack.current
    end
  end

  describe :size do
    it 'is zero at initialization' do
      assert @stack.size.zero?
    end

    it 'is incremented with push' do
      @stack.push(10)

      assert_equal 1, @stack.size
    end

    it 'is decremented on pop when an element is removed' do
      @stack.push(1)
      @stack.pop

      assert @stack.size.zero?
    end
  end

  describe :pop do
    it 'removes the current value from the stack' do
      @stack.push(1)
      @stack.pop

      assert_nil @stack.current
    end

    it 'after pop, the current value is the previous one' do
      @stack.push(1)
      @stack.push(2)
      @stack.pop

      assert_equal 1, @stack.current
    end
  end
end
