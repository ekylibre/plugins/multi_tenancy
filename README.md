# Ekylibre::MultiTenancy

This Gem provides deep integration with Apartment::Tenant for Ekylibre in a way that can be extended/used from plugins

Methods that can be safely called from outside of the MultiTenancyPlugin are tagged with the `@api` annotation.

The plugin hooks into Rails and Sidekiq by!:
- inserting a Rack middleware to handle tenant change for each request.
- inserting a Client and Server middleware to set and restore the tenant that scheduled the job when it is executed.
- adding the `switch_tenant`, `switch_tenant!`, `leave_tenant!` commands in the Rails console 
- providing a mixin module that should be included in the tests cases to automatically handle multitenancy for Minitest TestCase

## Limitations
As no memory separation is present between requests from different tenants, it is therefore not possible to share the container services between requests.
The current infrastructure of Ekylibre prevents the plugin system to be stateless regarding to the current tenant (some services need access to this information), it is mandatory to inject it for each request.

The lack of good immutable data structures in Ruby at the time of writing is then forcing us to duplicate the service container to ensure complete isolation between request.
This, however, is paid by a performance hit as no service can be reused.  
An other solution would be to be able to tag services as stateless and allow reuse only of such services (and duplicating others).  
The dependency container not having this feature yet, and the difficult and possibly catastrophic problems that can arise in case of a badly configured service
are additional reasons for waiting to implement this solution to see whether or not the performance hit related to recreating the services each requests is really problematic.

## Usage
All the services are accessible through the service container from the PluginSystem

The tenant name is taken in order of first present:
- The `TENANT` env variable
- Defaults to `test` when the application is `test`
- The `HTTP_X_TENANT` HTTP header if the `ELEVATOR` env var is `header`
- The leftmost part of the hostname

### Services
#### TenantSwitcher
Responsible of switching between tenants. Prefer the use of `switch` by providing a block over the use of `switch!`
Each use of `switch!` should be matched by the use of `leave!` to not leave traces behind.
Whenever a block given to `switch` finishes, the TenantSwitcher automatically `leave!` all the remaining `switch!` in order to continue with a clean state.

#### TenantStack
The tenant Stack is responsible of keeping track of all tenant switches that happened in the current context. Use the `current` method to get the current tenant. The stack is automatically managed by the TenantSwitcher.
No reference to a Tenant returned by the stack should be kept in a variable that is not local. If access to the current  tenant is needed is different places of the application, the TenantStack should be provided and used to get the current tenant.

#### TenantRepository
The TenantRepository keeps the list of currently existing tenants at the time when the request started.

#### PrivateDirectoryManager
This service handles all operations to manage the private directory of all tenants.

#### TenantManager
This service handles creation and destruction of tenants.
